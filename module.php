<?php

include "config/config.php";
include "config/helper.php";

$session = session_get($config);

// print_r($session);

if (!$session) {
    header('location:index.php');
}
$module = $_GET['module'] ? $_GET['module'] : NULL;
$action = $_GET['action'] ? $_GET['action'] : NULL;

$module_name = isset($_GET['module']) ? str_replace('_', ' ', ucwords($_GET['module'])) : 'Module';
$action_name = isset($_GET['action']) ? str_replace('_', ' ', ucwords($_GET['action'])) : NULL;

?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title> <?= $module_name ?> | Oprekan Module </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Oprekan Module FIST Punya" name="description" />
    <meta content="nsmajid" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <!-- Bootstrap Css -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="assets/css/app.min.css" rel="stylesheet" type="text/css" />

</head>

<body data-topbar="colored" data-layout="horizontal" data-layout-size="boxed">

    <!-- Begin page -->
    <div id="layout-wrapper">

        <header id="page-topbar">
            <div class="navbar-header">
                <div class="container-fluid">
                    <div class="float-right">

                        <div class="dropdown d-inline-block ml-2">
                            <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-search-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="mdi mdi-magnify"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0" aria-labelledby="page-header-search-dropdown">

                                <form class="p-3">
                                    <div class="form-group m-0">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Search ..." aria-label="Recipient's username">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary" type="submit"><i class="mdi mdi-magnify"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>



                        <div class="dropdown d-inline-block">
                            <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img class="rounded-circle header-profile-user" src="assets/images/users/avatar-1.jpg" alt="Header Avatar">
                                <span class="d-none d-sm-inline-block ml-1"><?=$session['username']?></span>
                                <i class="mdi mdi-chevron-down d-none d-sm-inline-block"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <!-- item-->
                                <a class="dropdown-item" href="#"><i class="mdi mdi-face-profile font-size-16 align-middle mr-1"></i> Profile</a>
                                <a class="dropdown-item" href="./dist/" target="_blank"><i class="mdi mdi-credit-card-outline font-size-16 align-middle mr-1"></i> Template</a>
                                <a class="dropdown-item" href="#"><i class="mdi mdi-account-settings font-size-16 align-middle mr-1"></i> Settings</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="logout.php"><i class="mdi mdi-logout font-size-16 align-middle mr-1"></i> Logout</a>
                            </div>
                        </div>
                    </div>

                    <!-- LOGO -->
                    <div class="navbar-brand-box">
                        <a href="module.php?module=home" class="logo logo-dark">
                            <span class="logo-sm">
                                <img src="assets/images/logo-sm-dark.png" alt="" height="22">
                            </span>
                            <span class="logo-lg">
                                <img src="assets/images/logo-dark.png" alt="" height="20">
                            </span>
                        </a>

                        <a href="module.php?module=home" class="logo logo-light">
                            <span class="logo-sm">
                                <img src="assets/images/logo-sm-light.png" alt="" height="22">
                            </span>
                            <span class="logo-lg">
                                <img src="assets/images/logo-light.png" alt="" height="20">
                            </span>
                        </a>
                    </div>

                    <button type="button" class="btn btn-sm mr-2 font-size-16 d-lg-none header-item waves-effect waves-light" data-toggle="collapse" data-target="#topnav-menu-content">
                        <i class="fa fa-fw fa-bars"></i>
                    </button>

                    <div class="topnav">
                        <nav class="navbar navbar-light navbar-expand-lg topnav-menu">

                            <div class="collapse navbar-collapse" id="topnav-menu-content">
                                <ul class="navbar-nav">
                                    <li class="nav-item">
                                        <a class="nav-link" href="module.php?module=home">
                                            Dashboard
                                        </a>
                                    </li>

                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-uielement" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            CRUD <div class="arrow-down"></div>
                                        </a>
                                        <div class="dropdown-menu dropdown-mega-menu-xl px-2" aria-labelledby="topnav-uielement">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="dropdown-item-text font-weight-semibold font-size-16">
                                                        <div class="d-inline-block icons-sm mr-1"><i class="uim uim-box"></i></div> CRUD
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-lg-5">
                                                            <a href="module.php?module=agama" class="dropdown-item">Agama <div class="badge badge-info">Basic </div> </a>
                                                            <a href="module.php?module=user" class="dropdown-item">User <div class="badge badge-info">Login </div></a>
                                                            <a href="module.php?module=user_profile" class="dropdown-item">User Profile <div class="badge badge-info">Form Type </div> </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle arrow-none" href="#" id="topnav-uielement" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Elements <div class="arrow-down"></div>
                                        </a>
                                        <div class="dropdown-menu dropdown-mega-menu-xl px-2" aria-labelledby="topnav-uielement">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="dropdown-item-text font-weight-semibold font-size-16">
                                                        <div class="d-inline-block icons-sm mr-1"><i class="uim uim-box"></i></div> UI Elements
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-lg-5">
                                                            <a href="ui-alerts.html" class="dropdown-item">Alerts</a>
                                                            <a href="ui-badge.html" class="dropdown-item">Badge</a>
                                                            <a href="ui-buttons.html" class="dropdown-item">Buttons</a>
                                                            <a href="ui-cards.html" class="dropdown-item">Cards</a>
                                                            <a href="ui-dropdowns.html" class="dropdown-item">Dropdowns</a>
                                                            <a href="ui-navs.html" class="dropdown-item">Navs</a>
                                                        </div>
                                                        <div class="col-lg-5">
                                                            <div>
                                                                <a href="ui-tabs-accordions.html" class="dropdown-item">Tabs &amp; Accordions</a>
                                                                <a href="ui-modals.html" class="dropdown-item">Modals</a>
                                                                <a href="ui-images.html" class="dropdown-item">Images</a>
                                                                <a href="ui-progressbars.html" class="dropdown-item">Progress Bars</a>
                                                                <a href="ui-pagination.html" class="dropdown-item">Pagination</a>
                                                                <a href="ui-popover-tooltips.html" class="dropdown-item">Popover & Tooltips</a>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>


                                            </div>

                                        </div>
                                    </li>



                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>


        </header>
        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            <div class="page-content">

                <!-- Page-Title -->
                <div class="page-title-box">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <div class="col-md-8">
                                <h4 class="page-title mb-1"><?= $module_name ?></h4>
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="?module=home">Oprekan Module</a></li>

                                    <?php
                                    if ($action_name) {
                                    ?>
                                        <li class="breadcrumb-item"><a href="?module=<?= $module ?>"> <?= $module_name ?></a></li>
                                        <li class="breadcrumb-item active"><?= $action_name ?></li>

                                    <?php
                                    } else {
                                    ?>
                                        <li class="breadcrumb-item active"><?= $module_name ?></li>
                                    <?php
                                    }

                                    ?>
                                </ol>
                            </div>

                        </div>

                    </div>
                </div>
                <!-- end page title end breadcrumb -->

                <div class="page-content-wrapper">
                    <div class="container-fluid">
                        <?php session_flash("notif"); ?>

                        <div class="row">
                            <?php include "content.php"; ?>


                        </div>
                        <!-- end row -->

                    </div>
                    <!-- end container-fluid -->
                </div>
                <!-- end page-content-wrapper -->
            </div>
            <!-- End Page-content -->


            <footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            2020 © Xoric.
                        </div>
                        <div class="col-sm-6">
                            <div class="text-sm-right d-none d-sm-block">
                                Crafted with <i class="mdi mdi-heart text-danger"></i> by Themesdesign
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <!-- end main content-->

    </div>
    <!-- END layout-wrapper -->

    <!-- Right bar overlay-->
    <div class="rightbar-overlay"></div>

    <!-- JAVASCRIPT -->
    <script src="assets/libs/jquery/jquery.min.js"></script>
    <script src="assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/libs/metismenu/metisMenu.min.js"></script>
    <script src="assets/libs/simplebar/simplebar.min.js"></script>
    <script src="assets/libs/node-waves/waves.min.js"></script>

    <script src="https://unicons.iconscout.com/release/v2.0.1/script/monochrome/bundle.js"></script>


    <script src="assets/js/app.js"></script>

</body>

</html>
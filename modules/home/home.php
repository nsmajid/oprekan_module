<div class="col-lg-12">
    <div class="card">
        <div class="card-body">
            <h4 class="header-title">Home</h4>
            <p class="card-title-desc">Exampel of Module.</p>
            <div class="row">
                <div class="col-xl-6">
                    <div>

                        <h5 class="font-size-16 mb-3">Example 1 :</h5>
                        <h3>Xoric - Bootstrap 4 Admin Template <span class="badge badge-soft-primary">v1.0</span></h3>

                        <div class="mt-4 mt-xl-5">
                            <h5 class="font-size-16 mb-3">Example 2 :</h5>
                            <button type="button" class="btn btn-primary mr-2">
                                Notifications <span class="badge badge-success ml-1">3</span>
                            </button>
                            <button type="button" class="btn btn-info mr-2">
                                Messages <span class="badge badge-primary ml-1">5</span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3">
                    <div class="mt-4 mt-xl-0">
                        <h5 class="font-size-16 mb-3">Example 3 :</h5>
                        <ul class="social-icon list-inline">

                            <li class="list-inline-item">
                                <a href="#" class="social-list-item">
                                    <div>
                                        <i class="mdi mdi-facebook"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#" class="social-list-item">
                                    <div>
                                        <i class="mdi mdi-twitter"></i>
                                    </div>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#" class="social-list-item">
                                    <div>
                                        <i class="mdi mdi-email-outline"></i>
                                        <span class="badge badge-pill badge-success">3</span>
                                    </div>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#" class="social-list-item">
                                    <div>
                                        <i class="mdi mdi-google-plus"></i>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
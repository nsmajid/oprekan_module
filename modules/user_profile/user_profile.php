<?php
switch ($action) {

    default:
?>

        <div class="col-xl-12">
            <div class="card">
                <div class="card-body">
                    <div class="float-right ml-2">
                        <a href="?module=<?= $module ?>&action=add" class="btn btn-primary btn-sm">Add <i class="mdi mdi-plus ml-1"> </i></a>
                    </div>

                    <h4 class="header-title"> DATA <?= $module_name ?></h4>

                    <br>
                    <div class="table-responsive">
                        <table class="table mb-0">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Nama </th>
                                    <th>Username </th>
                                    <th>Agama </th>
                                    <th>Jenis Kelamin </th>
                                    <th>Alamat </th>
                                    <th>Kode POS </th>
                                    <th>Telepon </th>
                                    <th>Tempat Lahir </th>
                                    <th>Tanggal Lahir </th>
                                    <th>Golongan Darah</th>
                                    <th>Options</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $query = mysqli_query($connection, "SELECT * FROM user_profile LEFT JOIN user ON user_profile.id_user=user.id_user LEFT JOIN agama ON user_profile.id_agama=agama.id_agama ");

                                $no = 1;
                                while ($row = mysqli_fetch_array($query)) {
                                ?>
                                    <tr>
                                        <th scope="row"><?= $no++ ?></th>
                                        <td><?= $row['nama'] ?></td>
                                        <td><?= $row['username'] ?></td>
                                        <td><?= $row['nama_agama'] ?></td>
                                        <td><?= $row['jenis_kelamin'] ?></td>
                                        <td><?= $row['alamat'] ?></td>
                                        <td><?= $row['kode_pos'] ?></td>
                                        <td><?= $row['telepon'] ?></td>
                                        <td><?= $row['tempat_lahir'] ?></td>
                                        <td><?= $row['tanggal_lahir'] ?></td>
                                        <td><?= $row['golongan_darah'] ?></td>

                                        <td>
                                            <div class="btn-group" role="group">

                                                <a href="?module=<?= $module ?>&action=edit&id=<?= $row['id_user_profile']; ?>" type="button" class="btn btn-outline-secondary btn-sm" data-toggle="tooltip" data-placement="top" title="Edit">
                                                    <i class="mdi mdi-pencil"></i>
                                                </a>
                                                <a href="<?= $query_model ?>&action=delete&id=<?= $row['id_user_profile']; ?>" onclick="window.alert('Apa Anda yakin??')" type="button" class="btn btn-outline-secondary btn-sm" data-toggle="tooltip" data-placement="top" title="Delete">
                                                    <i class="mdi mdi-trash-can"></i>
                                                </a>
                                            </div>
                                        </td>

                                    </tr>

                                <?php
                                }
                                ?>

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        <?php
        break;

        // Form Tambah college_schedule
    case "add":
        ?>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">

                        <h4 class="header-title"><?= $action_name ?> <?= $module_name ?></h4>
                        <p class="card-title-desc"></p>

                        <form class="custom-validation" action="<?= $query_model ?>&action=insert" method="POST">
                            <div class="form-group">
                                <label>Nama</label>
                                <input type="text" name="nama" class="form-control" required placeholder="Type something" />
                            </div>
                            <div class="form-group">
                                <label>User</label>
                                <select class="form-control custom-select" name="id_user" requireed>
                                    <option value="">-Pilih-</option>
                                    <?php
                                    $query_user = mysqli_query($connection, "SELECT * FROM user ");

                                    while ($row_user = mysqli_fetch_array($query_user)) {
                                    ?>
                                        <option value="<?= $row_user['id_user'] ?>"><?= $row_user['username'] ?></option>

                                    <?php
                                    }
                                    ?>


                                </select>
                            </div>
                            <div class="form-group">
                                <label>Agama</label>
                                <select class="form-control custom-select" name="id_agama" requireed>
                                    <option value="">-Pilih-</option>
                                    <?php
                                    $query_agama = mysqli_query($connection, "SELECT * FROM agama ");

                                    while ($row_agama = mysqli_fetch_array($query_agama)) {
                                    ?>
                                        <option value="<?= $row_agama['id_agama'] ?>"><?= $row_agama['nama_agama'] ?></option>

                                    <?php
                                    }
                                    ?>


                                </select>
                            </div>

                            <div class="form-group">
                                <h5 class="font-size-14 mb-3">Jenis Kelamin</h5>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="laki_laki" value="L" name="jenis_kelamin" class="custom-control-input">
                                    <label class="custom-control-label" for="laki_laki">Laki-laki</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="perempuan" value="P" name="jenis_kelamin" class="custom-control-input">
                                    <label class="custom-control-label" for="perempuan">Perempuan</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Alamat</label>
                                <div>
                                    <textarea required name="alamat" class="form-control" rows="5"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Kode POS</label>
                                <input type="number" name="kode_pos" maxlength="6" class="form-control" required placeholder="Type something" />
                            </div>
                            <div class="form-group">
                                <label>Telepone</label>
                                <input type="text" name="telepon" class="form-control" required placeholder="Type something" />
                            </div>
                            <div class="form-group">
                                <label>Tempat Lahir</label>
                                <input type="text" name="tempat_lahir" class="form-control" required placeholder="Type something" />
                            </div>
                            <div class="form-group">
                                <label>Tanggal Lahir</label>
                                <input type="date" name="tanggal_lahir" class="form-control" required placeholder="Type something" />
                            </div>
                            <div class="form-group">
                                <h5 class="font-size-14 mb-3">Golongan Darah</h5>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="A" value="A" name="golongan_darah" class="custom-control-input">
                                    <label class="custom-control-label" for="A">A</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="B" value="B" name="golongan_darah" class="custom-control-input">
                                    <label class="custom-control-label" for="B">B</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="AB" value="AB" name="golongan_darah" class="custom-control-input">
                                    <label class="custom-control-label" for="AB">AB</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="O" value="O" name="golongan_darah" class="custom-control-input">
                                    <label class="custom-control-label" for="O">O</label>
                                </div>
                            </div>
                            <div class="form-group mb-0">
                                <div>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                        Submit
                                    </button>
                                    <button type="button" onclick="window.location.href = '?module=<?= $module ?>'" class="btn btn-secondary waves-effect">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        <?php
        break;

        // Form Edit Kriteria
    case "edit":
        $query = mysqli_query($connection, "SELECT * FROM user_profile WHERE id_user_profile='$_GET[id]'");
        $row = mysqli_fetch_array($query);
        ?>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">

                        <h4 class="header-title"><?= $action_name ?> <?= $module_name ?></h4>
                        <p class="card-title-desc"></p>

                        <form class="custom-validation" action="<?= $query_model ?>&action=update" method="POST">
                            <div class="form-group">
                                <label>Nama</label>
                                <input type="text" name="nama" class="form-control" required placeholder="Type something" value="<?= $row['nama'] ?>" />
                            </div>
                            <div class="form-group">
                                <label>User</label>
                                <select class="form-control custom-select" name="id_user" requireed>
                                    <option value="">-Pilih-</option>
                                    <?php
                                    $query_user = mysqli_query($connection, "SELECT * FROM user ");

                                    while ($row_user = mysqli_fetch_array($query_user)) {
                                    ?>
                                        <option value="<?= $row_user['id_user'] ?>" <?= $row['id_user'] == $row_user['id_user'] ? 'selected' : NULL ?>><?= $row_user['username'] ?></option>

                                    <?php
                                    }
                                    ?>


                                </select>
                            </div>
                            <div class="form-group">
                                <label>Agama</label>
                                <select class="form-control custom-select" name="id_agama" requireed>
                                    <option value="">-Pilih-</option>
                                    <?php
                                    $query_agama = mysqli_query($connection, "SELECT * FROM agama ");

                                    while ($row_agama = mysqli_fetch_array($query_agama)) {
                                    ?>
                                        <option value="<?= $row_agama['id_agama'] ?>" <?= $row['id_agama'] == $row_agama['id_agama'] ? 'selected' : NULL ?>><?= $row_agama['nama_agama'] ?></option>

                                    <?php
                                    }
                                    ?>


                                </select>
                            </div>

                            <div class="form-group">
                                <h5 class="font-size-14 mb-3">Jenis Kelamin</h5>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="laki_laki" value="L" name="jenis_kelamin" class="custom-control-input" <?= $row['jenis_kelamin'] == 'L' ? 'checked' : NULL ?>>
                                    <label class="custom-control-label" for="laki_laki">Laki-laki</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="perempuan" value="P" name="jenis_kelamin" class="custom-control-input" <?= $row['jenis_kelamin'] == 'P' ? 'checked' : NULL ?>>
                                    <label class="custom-control-label" for="perempuan">Perempuan</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Alamat</label>
                                <div>
                                    <textarea required name="alamat" class="form-control" rows="5"><?= $row['alamat'] ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Kode POS</label>
                                <input type="number" name="kode_pos" maxlength="6" class="form-control" required placeholder="Type something" value="<?= $row['kode_pos'] ?>" />
                            </div>
                            <div class="form-group">
                                <label>Telepone</label>
                                <input type="text" name="telepon" class="form-control" required placeholder="Type something" value="<?= $row['telepon'] ?>" />
                            </div>
                            <div class="form-group">
                                <label>Tempat Lahir</label>
                                <input type="text" name="tempat_lahir" class="form-control" required placeholder="Type something" value="<?= $row['tempat_lahir'] ?>" />
                            </div>
                            <div class="form-group">
                                <label>Tanggal Lahir</label>
                                <input type="date" name="tanggal_lahir" class="form-control" required placeholder="Type something" value="<?= $row['tanggal_lahir'] ?>" />
                            </div>
                            <div class="form-group">
                                <h5 class="font-size-14 mb-3">Golongan Darah</h5>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="A" value="A" name="golongan_darah" class="custom-control-input" <?= $row['golongan_darah'] == 'A' ? 'checked' : NULL ?>>
                                    <label class="custom-control-label" for="A">A</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="B" value="B" name="golongan_darah" class="custom-control-input" <?= $row['golongan_darah'] == 'B' ? 'checked' : NULL ?>>
                                    <label class="custom-control-label" for="B">B</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="AB" value="AB" name="golongan_darah" class="custom-control-input" <?= $row['golongan_darah'] == 'AB' ? 'checked' : NULL ?>>
                                    <label class="custom-control-label" for="AB">AB</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="O" value="O" name="golongan_darah" class="custom-control-input" <?= $row['golongan_darah'] == 'O' ? 'checked' : NULL ?>>
                                    <label class="custom-control-label" for="O">O</label>
                                </div>
                            </div>
                            <div class="form-group mb-0">
                                <input type="hidden" name="id_user_profile" class="form-control" required placeholder="Type something" value="<?= $row['id_user_profile'] ?>" />

                                <div>
                                    <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                        Submit
                                    </button>
                                    <button type="button" onclick="window.location.href = '?module=<?= $module ?>'" class="btn btn-secondary waves-effect">
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>


    <?php
        break;
}
    ?>